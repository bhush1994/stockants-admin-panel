import { createRouter, createWebHistory } from 'vue-router'
import LogIn from '../views/LogIn.vue'
import HotControls from '@/views/HotControls.vue'
import DashBoard from '@/components/DashBoard.vue'
import UserManagement from '@/views/UserManagement.vue'
import PlanManagement from '@/views/PlanManagement.vue'
import PageNotFound from '@/components/PageNotFound.vue'
//import { access } from '../mixins/constant.js'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      name: 'DashBoard',
      component: DashBoard,
      path:'/dashboard',
      // meta: {
      //     access: [access.ADMIN]
      //   }
  },
  {
      name: 'HotControls',
      component: HotControls,
      path:'/hotcontrols',
      // meta: {
      //     access: [access.ADMIN]
      //   }
  },
  {
      name: 'LogIn',
      component: LogIn,
      path:'/',
      // meta: {
      //     access: [access.ADMIN]
      //   }
  },
  {
      name: 'UserManagement',
      component: UserManagement, 
      path: '/user-management',
      // meta: {
      //     access: [access.ADMIN]
      //   }
    },
    {
      name: 'PlanManagement',
      component: PlanManagement, 
      path: '/plan-management',
      // meta: {
      //     access: [access.ADMIN]
      //   }
    },

    {
      name: 'PageNotFound',
      component: PageNotFound, 
      path: '/:pathMatch(.*)*',
      // meta: {
      //     access: [access.ADMIN]
      //   }
    },

    
  ]
})

export default router
