export var HelperNetworkCallBack = {

    data(){
      return{
        quarter_list: [
          {
            value: 1,
            label: "1st Quarter",
            quarter_value: 1
          },
          {
            value: 2,
            label: "2nd Quarter",
            quarter_value: 2
          },
          {
            value: 3,
            label: "3rd Quarter",
            quarter_value: 3
          },
          {
            value: 4,
            label: "4th Quarter",
            quarter_value: 4
          }
        ],

        start_year_for_week_list: [
          {
            value: new Date().getFullYear() - 1,
            label: new Date().getFullYear() - 1
          },
          {
            value: new Date().getFullYear(),
            label: new Date().getFullYear()
          }
        ],

        end_year_for_week_list: [
          {
            value: new Date().getFullYear() - 1,
            label: new Date().getFullYear() - 1
          },
          {
            value: new Date().getFullYear(),
            label: new Date().getFullYear()
          },
          {
            value: new Date().getFullYear() + 1,
            label: new Date().getFullYear() + 1
          }
        ],

        month_list: [
          {
            value: 1,
            label: "Jan"
          },
          {
            value: 2,
            label: "Feb"
          },
          {
            value: 3,
            label: "Mar"
          },
          {
            value: 4,
            label: "Apr"
          },
          {
            value: 5,
            label: "May"
          },
          {
            value: 6,
            label: "Jun"
          },
          {
            value: 7,
            label: "Jul"
          },
          {
            value: 8,
            label: "Aug"
          },
          {
            value: 9,
            label: "Sep"
          },
          {
            value: 10,
            label: "Oct"
          },
          {
            value: 11,
            label: "Nov"
          },
          {
            value: 12,
            label: "Dec"
          }
        ],

        start_year_list: [
          {
            value: new Date().getFullYear() - 1,
            label: new Date().getFullYear() - 1
          },
          {
            value: new Date().getFullYear(),
            label: new Date().getFullYear()
          },
          {
            value: new Date().getFullYear() + 1,
            label: new Date().getFullYear() + 1
          },
          {
            value: new Date().getFullYear() + 2,
            label: new Date().getFullYear() + 2
          }
        ],

        end_year_list: [
          {
            value: new Date().getFullYear() - 1,
            label: new Date().getFullYear() - 1
          },
          {
            value: new Date().getFullYear(),
            label: new Date().getFullYear()
          },
          {
            value: new Date().getFullYear() + 1,
            label: new Date().getFullYear() + 1
          },
          {
            value: new Date().getFullYear() + 2,
            label: new Date().getFullYear() + 2
          }
        ],

        loader_default: {
          text: '',
          color: '#2d2e8d',
          textColor: '#000',
          maskColor: 'rgba(255, 255, 255, 0.8)',
          zlevel: 0,

          // Font size. Available since `v4.8.0`.
          fontSize: 12,
          // Show an animated "spinner" or not. Available since `v4.8.0`.
          showSpinner: true,
          // Radius of the "spinner". Available since `v4.8.0`.
          spinnerRadius: 10,
          // Line width of the "spinner". Available since `v4.8.0`.
          lineWidth: 5,
          // Font thick weight. Available since `v5.0.1`.
          fontWeight: 'normal',
          // Font style. Available since `v5.0.1`.
          fontStyle: 'normal',
          // Font family. Available since `v5.0.1`.
          fontFamily: 'Montserrat'
        }
      }
    },

    methods: {
      saveRolesToLocalStorage(roles){
        if(roles.length){
          localStorage.setItem('roles', JSON.stringify(roles));
        }else {
          localStorage.setItem('roles', JSON.stringify(['All']));
        }
      },

      saveProfileToLocalStorage(profile){
        for (var key in profile) {
          if (profile.hasOwnProperty(key)) {
            profile[key] = profile[key] ? profile[key] : '';
          }
        }
        localStorage.setItem('profile', JSON.stringify(profile));
      },

      saveUserIdToLocalStorage(user_id){
        if(user_id){
          localStorage.setItem('user_id', JSON.stringify(user_id));
        }else{
          localStorage.clear();
          this.$router.push('/');
        }
      },

      saveSubscribedPlanDetailsToLocalStorage(plan){
        for (var key in plan) {
          if (plan.hasOwnProperty(key)) {
            plan[key] = plan[key] ? plan[key] : '';
          }
        }
        localStorage.setItem('plan_details', JSON.stringify(plan));
      },

      getDifferenceBetweenTwoDays(created_date, current_date) {
        //set time to 12:00 Am to because getting time in created date and current date
        //that returns if recommnedation created on 3PM then on second date when it's 3PM then it returns 1 day
        var createdDate = new Date(created_date).setHours(0, 0, 0, 0);
        var currentDate = new Date(current_date).setHours(0, 0, 0, 0);

        const diffTime = Math.abs(currentDate - createdDate);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        //if created date and current date is same then return 0
        if(new Date(createdDate).toLocaleDateString() == new Date(currentDate).toLocaleDateString()){
          return 0;
        }else{
          return diffDays;
        }
      },

      checkQuarter(date) {
        let d = new Date(date);
        let quarter = '';

        if(d.getMonth() == 0 || d.getMonth() == 1 || d.getMonth() == 2){
          quarter = 4;
        }else if(d.getMonth() == 3 || d.getMonth() == 4 || d.getMonth() == 5){
          quarter = 1;
        }else if(d.getMonth() == 6 || d.getMonth() == 7 || d.getMonth() == 8){
          quarter = 2;
        }else if(d.getMonth() == 9 || d.getMonth() == 10 || d.getMonth() == 11){
          quarter = 3;
        }

        return quarter;
      },

      divideRating(rating){
        if(rating < 10){
          return 0.5;
        }else if(rating >= 10 && rating <= 20){
          return 1;
        }else if(rating >= 20 && rating <= 30){
          return 1.5;
        }else if(rating > 30 && rating < 40){
          return 2;
        }else if(rating >= 40 && rating <= 50){
          return 2.5;
        }else if(rating >= 50 && rating <= 60){
          return 3;
        }else if(rating >= 60 && rating <= 70){
          return 3.5;
        }else if(rating >= 70 && rating <= 80){
          return 4;
        }else if(rating >= 80 && rating <= 90){
          return 4.5;
        }else if(rating >= 90 && rating <= 100){
          return 5;
        }else{
          return 0.5;
        }
      },

      //calculate protential return by target price(reco price) and current price
      getPotentailReturn(target_price, current_price, reco_type) {
        target_price = Number(target_price);
        current_price = Number(current_price);

        if (target_price && current_price && reco_type == 'Buy') {
          return (((target_price - current_price) / current_price) * 100).toFixed(2);
        }else if (target_price && current_price && reco_type == 'Sell') {
          return (((current_price - target_price) / current_price) * 100).toFixed(2);
        }else if(target_price && current_price && target_price > current_price && reco_type == 'Hold'){
          return (((target_price - current_price) / current_price) * 100).toFixed(2);
        }else if(target_price && current_price && target_price < current_price && reco_type == 'Hold'){
          return (((current_price - target_price) / current_price) * 100).toFixed(2);
        }

        return 0;
      },

      base64Logo(){
          var base64_logo_img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABMwAAAGBCAYAAACaSi12AAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR4nO3dv3Ic17U37PFXziG9N0AcvfkMXXJOuMqOhVMoOxWQjEJSSOxMVGYnIyr0JABTq1gHjO0qkfmrMoELkMErkHgF+mrTq3laMAHMDGbv3t39PFUoSTaJ+dM9Pd2/XnutX/z0008TAAAAAODf/j/vAwAAAAD8L4EZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAICWX3ozAPKYzhZ7k8nkg8lkcr/1z0n8++yGBz2fTCY/xr9fxk/671fp5+L8+Mcb/i4AAAB39IuffvrJewhwR9PZYncymezFz/1bArG7ehPh2Yv0c3F+/ML2AwAA2B6BGcCGprNFCsYOJ5PJ/mQyudfx+/g8ArSzi/Pjy46fC8DWRLXut5ne0d+46VCfy6ODVIn9Q8En9nz35Nl+X94fAMqwJDOj6WzRXEinE72dwb5Q+uS/L86Pz657vsvl8jSdp87n88e26vtFJdmjSkKytk/i56vpbPFyMpmcXpwfn9bz9ABgZaXDq09SSLd78kzLAwDeEZhlEHdCTyu7mIant4RlKST7NP49hWbClpb4XD+KUKp2D9LPdLZ4MplM3v7oewZAj3RR7bUf5+8A8JYpmVsWVWXfCsuozHmEPe+1XC7TfvtF6/87WS6X923Efwdl09niRXyu+xCWte3Edr2czhaPp7PFB/U8NQD4T7Ecs4vvW0syAfgZgdkWRVh2MpgXxFCkBvF711UYRTD25D3/14vlcjnagCUtvZzOFmcRlD2o4CndRTs4uzY4BYAKdBVcfRJhHQC8JTDbkuhr9L7QAbp0W1j2QTSKf1+PvZ2xLk2IUOlVDyvKbrMTPc5exDELAGpz2OHzUWUGwDsCs+15rLE/Fdq/OD9+9b6ndUtY1vhkuVzujWXDpiWLsfzyq4F/nlPF3KuoigWAKlweHex2XNWtChuAdwRmWxB9gT7t/QthaI5uGZWfKiJnK7zmUVSZTWeLtDT1cgDLL1eVAsGT6WyhwTEAtei6wmsWoR0ACMy2ZDQVOPRGCsuuDULaEzFXcC+GAgxWVFrdVm03VJ/GEk19WwDoWg3nG5ZlAvDWL70NW2GaIDW5LSzbvzIRcxWPh1ppZljHW6mqLoVm1/a7q0Ga9Jnpaby4pRoTgMyismuVyvfcDvUlpgaXRwfZznt2T54574EVCMxgON5Ez7JrvwBjIuYmwVeqMtubz+eD+nIVlv3MLKrsar4BsG7Quw4njgDdqqWy6+2yzN2TZ5cVPBfGzXkPdMySTBiGZhrmTWFZWnJ3dodlh4NaljmdLfaFZf9hpqcZAB2pqeG+oTgACMy2xB0ounQ+mUx2r5uG2ZLCtHt3eJ6D6dUXDf4FQ++XepqZEgZAMZdHB/fveI6ybQIzAARmW6Kkla48jcqyG/tOLZfL0y30BbkXSzp7LZrbn460wf+qvopQEQBKqC2guhchHgAjJjDbgovz41Rh9rL3L4Q+eRPN/Q9XCMserTER8zZDOHl8XElT4dqpwAOglBonU6oyAxg5gdn25JpiAlelJZj3b5qE2YiJmF9t8R3sdWCWpkBOJpOHFTyVPphlnEoJAG9VuByzUWOIB0BBpmRuSWq2Pp0tnm6xkgeuSlVljy/Oj1cadX6HiZg36XuFWQ0BUAo8f7xlKXd6n9PS0QcFn9f7PJrOFk9uq2IEgDuotZLr7bLM3ZNnt/WIBWCgBGbb9SgudC33YtvSkt/DWP57qy1MxByc6Wxx2FEA9Tq2xdlNU0yvE73E9uKCovSxZSeOayrNAMil5qWPjyzNBBgvgdkWpSqMuCh/IahgS15HULZu0HLXiZjX2e3xhi0d+qSK09NNQrK2mH6afp5MZ4vd1sl7qWOMKjMAsrg8Otiv/JzZskyAEdPDbMvi4vbRoF4UXXgdTf131w1ctjQR8zo19hi5VfQuK/XcUzXgr2Igw1Yn6KYKw4vz40cRXD7d5u++wY676wBkUnsgtROhHgAjJDDLIJqxl7qYZVjaQdna/ceWy+WhPnrvVSrw+fLi/HgvgvNsUrVXCuQmk8lvorddbgIzAHLoQxglMAMYKYFZPo+iuTes4nkKPzYNyib/DstSFdVJ5ne7d/v0dLb4oFCImILOoss+o4Jtt8B2mcVyUADYikzLMb/OsHUEZgAjJTDLJPr9HBaq/qCfUjXZl5PJ5L8uzo/377J8LyZinhV4F/rYx6rEie7RpkHnXcWxZq9AaOaCAYBt2vb3yutM/UotywQYKYFZRvqZ8R7ncffzV1FN9njVyZfXiYmYpwZNXGsv8+9/3lVY1ojQbD9zQO9iAYCtuDw6+CDD98qL3ZNnP0bV/rZpTQAwQgKzzOJCOscXN/3wOvrZHUUl2f3UtH3LPa7OMjb5v2qrTewLyR2YVRGKR/Ca87ncz/i7ARiXHMsxz678c5s+iZAPgBH5pY1dRLor9aqvEwZZSQrGLmM7/xjB0quo/MkmJmI+KLiJerUkM/qX5fzcPb1rheA2pYB+OlscZtondqazxf3cAw0AGIVtV5e92T151g7McvR03Y+KfgBGQmBWQApNprNF+pL9Z2VP7XkT7FTwXHrpLn3H7qqjiZh921dyV0WV6Bu3ricZQ1R310dgOls0VZn3V9jmzU2Cy5rC46GIYRvNwI3bqmV/bI7RXX43wW2iUuuTLb9R776P07LMy6OD8wzV9wKzwmJfuelc7sfdk2euY1jL5dFB833a/o69SfOd+iqWfTMiArNCUlXGdLb4fDKZfFXB00l9ju7UZJ5uFZqI+R/m83nf9pmsgdnF+XF1gVl6TtPZ4nWmyrq9ni7L5T2iAnMvfu7Hz8ZLpKazxST6NL6KnxcqEleXKjhjG+zFCfzGwXdsizfNdmhtDyf61CBHT8yr302nGc653y7LdMGcx+XRwe6V76SVAs/Lo4NJ63jXHPNe2E609qn7dzzP+aL5l9jfXrZWFr0Q2g6bwKygi/PjJ3Hnftt31daRvlD2XMT0V8GJmFe97OGblrMiKvdUyrt40UH14dpi+WgNjZQPW1VV60pLr3sz3CVCmcM4gczR+3AWP5/G472J41XaJ88ENj8X1ef7sT22HXLvROj2LnibzhYvY3ucqQgsr+Axb9u9UrctR2B29bzoLNNN6t5XmV0eHeS48ZUqb9b+LowKsuZzcZfvpPbx7mH87ufN8a6W8Ozy6KCa855WldW6NtrWpcRE2734rOZqy9Lsa2/PdS6PDt609rUaV59wBwKz8rruZ/ZYWNZfHU/E9AXwczVf+OcKzLZdsXenKpotujfkHpNRSXYYQyFKv86d2BfTT7pplI4jp2OucI5llo9im5Q+ljcn+V9FeHba9ZTfsYiw+kmBbf55zed5UfGx7RvHz68GIrsnzy4zLct8NIBlmZ1/78Z+8Djzzb1P4ufJ5dFB+uw9qSA4c96TQexPXX2vTtrnOq3w7InKs2EwJbOwuLue487aqpwY99tpwYmYV/XxAneskx1zVY7oYdYjKZiZzhansT98VcHJcXNC+e10tngR1Taj0doe/4oKiC5O6tvSRdvJdLa4HNu2KC1C6xcFtnkaRPOkvnfgZ0pUl932v9/FLC7O2UCqKIvw6l8FK+F3YkldClEf227DkarkLo8Ozir6Xp20znX+mao571DJRyVUmHWgw35mLy2H6a/lcvmkw+W8r+fzeR/vkuQMeGo+YU7b6jcZfq/jRw/Exfmjds+NCr2tdIqg5vGQK86ioix3JcVd3Ivg7HFsCzfWtqhwWNaH4DPHc7wpMMtxHNyPakHWEMHBaYc3b94GZ7Fk71D1T39FaN3lddGq0rnOt5dHBy9jn9MKoYcEZh2ppJ8ZPRETMR92+Gwtx/xP99KFcI19gCIY15x/hKInVlfLtjfxICrOUq+Zw6Hd1IkQ6lFPtkcTnB3GtnBivx0lKsPPYz+rWlzkbvu9OL9umV0KRC6PDnIMwTkUmK3n8ujgUSWDzyaxD6bqn6Pdk2duEPRMVAnWfEPwfdK5zr8ujw6+3D15psqxZyzJ7NZhNOEvZazL03otmvwXn4h5RV9PDHNf8HW5vBreSVUs0R/sf3oUlrWlm0eXEfj1Xiy/fBUn9X3bHunE/pVlmnc3nS1KVECcxzCnPoTNOT7ftwUelmV27PLoIMfE0m04ieWh9GM/2r08OnjVw7CsLVU4vnL86BeBWYc66Ge24wS4X5bL5W4FlUIv5/N5XysNcj/v3kxHZLiimfjlACqWU7D0PxEy9FZ8z77qsN/kNuxEtZnqiw3FfpC7MjzddN3vUWVmyeWYq/7/m3I+vYIIy2qe2v0wniMVuzw6uD+A79VGeg2v4jXRAwKzjkXfli8LPovH0U+DysVEzLMKqhPcfbvevb5f3NNvcVH+z55WlV3nYarO6uN3VSzBPBnQ9vi0r9uiS9FyI3dl+JuoLOvFDa24OMyxHPPG17978uxFptUcArNbxNK5msOyRppsaHtWKrZNiT6QJaXX8sJ+1w8CswpcnB+nL5SXhZ7JPZMye6PLiZiN1Oy/z/3LSlxIPFS5SRems8WjCpZr55KOfS/6FNRENVafl4pcp3fboktR8VniezNVlvWpaXmO78lVK/BzbI97KkSuF431+3Q8PDHNsD6xTYZ0E6ptJ/Y7x5HKCczqsV+wn9kncaFFpTqeiNnW98aUpe68n/hMUVKEMzX2hNmm3gQ1sT36UEmxKaHZCuL9KTF046iHk2W76F/WsCyzoOjP1Meb86eXRweOcZWIIGkMQ8deCM3qJjCrRAf9zL6Ku6BUpoKJmI1UXdbrasTCFxTpM5UuKDXyJKuoaBxyONNWfVAzgrCsMTN991YvClSGf35xftyr7+a4GNz2pMrXaQrmKn9w9+RZrotug3/er0+TmtvuDeBG8SBEcNnX/WhdO8LaugnMKtJBP7Mzd4vrUslEzMZQThrOCz5WM11Or0CyiLBsqMswr1NtUDOy8DKZGQTwfvG+5A7Lnl6cH/exb2YXzf6vep7hOViWeUUsxXxQ1ZNaz0PbtAqPB9Lgf1UzYW29BGaV6aCf2RhKXXshmvzXclHY++qyltLv6U707bhMF1AqOdmW2JfGOmSiuqCmUGP3Gn2qb+PPxZL83MFpCsv6+r7nqMRa97s91/mudgw/N4TvKMOcOhR9y2pYaVPaQ3306iQwq1PJfmYPYqoXHWqFZbWUHg9pn+gqhNyJC6h/xpS5R8IzNhUVizVMze1SNUFNa3uM1RPLz/8t9snc/QTP+xrMxAXgtpdjvtlgmaVlmZnFxL9tb+suPIg+bHRjzNelrskrJDCrUPQzK3lR8EXcKac7TyoqPX45oOqy9Hk6KxhAX2cWF1QpPGsqz/Yt22QNjwdyIXJXtQQ1Y+mtcp0dE7eLVX2msGwvzg37qIblmKmP2Y+ZVnDsxDLEsftgYBf7gosORMBecklvOr5+HS2RjiaTyW/i5yj+t6elW7uoMqvPL8f+BtQqXeRPZ4uvC5akpsfb7fEJWW8tl8vHlfXAGeLygtOKyrvvxfZ+u82ns0X6In4VPy8uzo9XamLMeMQNjS7337SPXv1uuN9RYNQENZ2dUKawu5Ipxl1LFep7PZzWuBUR3OauDE83e/Z7fm6WI0zatFrsLNPF+L4WJ4PrNyUE7UaJgpHzuNFxFkH6raIh/35cI+Xe1x8ZsFMXgVnFLs6PH8WFUokvoZ34spdqFxQTMb+o6Cl9PZ/PhxjYPKm4H8IsfpoAbRJ3wdsh2mXnz5Iula7keROPeXZTGBKBwV6c4Ja8I5yCmv2oHi0qqkK76G/zJk6gX11zIr0bIWapc4bGaTz2qBRaIv0mKst6e/yPyqsc79GmF5NnmZbPCleG523lYMYJq1wRoVTOAoJ0TD3cZJtGsHYa0yz3M1eZf5KWBO+ePHPuXwmBWf324wS5xJ38dBHyJAV1fX/T+iAmYtbUWPTNUEvQ0wXHdLZ42qNpdg/aAcR0tnjTulBuQrQhVIOernnh822m5/H0DqFU1u0QzcRLLcVM+9mji/Pjld6LuJB/ewLZWppWKjh70lFFR8ntMYnw/Mk64WAEmY8iyMx97nAv9fBadZ8ZkBITMfcHUHGcI0h6vmpVyFXpAvTy6OA8w7YTrmzmdevm4CT+uRvLOydxA6CrauZJpspB5z3Xyxk8v13avumxoy19zqPH3YuM3wP7hk/UQ2BWubjQTye9/1PomT6czhYvurhzPyYVNvlPDufz+ZCX5Na29HUdO+8J0V5fCdB6d2EVgcvKd9Ci+i6HyxqXlUUVS6kQ+3k6BmwaxMb+txfLFUv09yoe1MT2KHVD6U1sj7W/i+Nz9SgG+pwWWD56OKZ+ZjGtNfd7ejSQpa41Lcds//0cF7mHlmWupKlgPt09ebbSecvl0cH9OPaWPofb+v4bVUMrn/dcHh1s+ym8+9W7J89qO8bkWuX0elthWSP9rug1lis02xOY1UPT/x6IE+avCz7TU9Ov8qk0LHs+n88HfaIXF5FfVvBUtqXphdYME/gxhgkcGiYwGI8KHSc+vzg/3kqfpPi+2osT1NxKV8SW2h6pqmz3rjeu0vZM2zWaF+f0YCwTgOMGZu6L9s+HULEXExNzfF62EZjl8EksKeN66RwsLTV7tGpYNvl3OPFq9+RZ2p/+K27ulLJjWmZRub5HDrcZljXid+5lGiymT2pFBGY9EcskS03p2HGXLKuaJmJOmkqGCp5Hdhfnx48LT7spaScu5E4mk8kP09niTHjWeyU+l6mSZat3MaPa7H6B0OxeBBillKgue3pxfrzViYgRvvwq87TgwX+HRE/Zk8wP83Tbn8cO5aguO7/rhW8ENbmOTXqZvV867/rV7smzx3fZfqk6a/fkWYmbAG16O5eT49rodc5Kutifs5wbRGUlFRCY9ct+5hPetlksO2CLKpyImewPfCnmVSU/R136pBWencbFHj0RQVDuXllf56pkicCnxGetyBLJ2B65q8teXpwfZwmeIsTM+V4NOjCLCrrcNxKf5tr+pUWlVY4KiW0dr3JtS4HZf2p6R22tbcTuybPTgqGZCrMCMlbyZS8Aif0xx7mOG96VEJj1SNOXpOAz/rTw3ftBWy6X+5VNxJzEVMxRjS6Oz9HYTmpTSPvtdLa49Jnujdz76MvcA14KhDSTuLlT4i5s7s/N69zbPMLRXO0ddoa6LLPQRMzzwud3ueXal7d18WtZZhlba7R+VYQUJdpsuNlYRq7ArFRBQI5jin2vEgKznokT3qcFn/WTsfQmySkmYtZWsXc+n89HORE1mimXLOmvRapYOhGc1S16SJZo1J5dfGe97PNrie2Re/rnxgMX1hEhqeVo63mRudrzbagwkMnHjVzLMVduln6TWKKVq/pVldm/pfd3P0dY1khLPAu02RCAsopRFR+MjcCsn0r3MzvVB2lz0eQ/993pdb0Z+0ldXMiPMTSbXAnOnNzXJ/c2+TIqLUvJHc7lfr9y//6nhSci5hqWMLi74dGaImfP0bffxUMKyzIux9z2ZyRXldkob0S+x+NtBZy3yF7FnPn3k1epJbUlz6ko7Jfe8P5JJ1ZRHVJq0uIsGtWrSNlM7rvTmzicz+ejP7in0CyFRhUGmqWk/fJ/prPF81IVLqwkZ0DzpvSo8hTOTWeLpxn7N6bm//djCWgOuQOzotM+47j3OMP30qCq0eM9ytlz9E1Ulg3tuzjX52XbVfpnmbbvLPVjKhQW1So1Wi/yPZOqBS+PDs4FW1xjP4X4OSsdJ/9btfoLG2GYVJj1VKHeMG36mW1guVzmvju9iS/n87kpqCEqO+4PeHrmKlI1gGqzCkQ1b87lf6cdBaO5Q6Es+26B7fG0o8Akx8XsTixf7b0438ndc3Q/Y8jbpRznpq+32TR+8u8L3JznQWP/Li16E6DClifUY6f0TUKGR2DWYx30MzvRz2x1y+XyUYUTMZ/P5/PSJzLVSxesF+fH9zM2xO6Dnag2s390K/eytk5OHCMUyhlK53rfBrk9Mi5H631gFuc5ubfLUeFluEXEpLscNwlz7a/PM/3eMd9gfhMN+UvKehP48uhA8/XMokIrl08vjw6EqmxMYNZ/JfuZJWf6md0uJmJ+VdnTOres9mbREPtXBZqU1+yL6NtDN3LelDjvePlXzv0qVxVYzu3xuqsKo4wBZq9vqkWFXO52F5/HDc8hylVZletiOlfQMovwcIyKr2CI5a+5hplQTq5BHJMIzV6M+HPJHQjMei6W1hxmPsi03VP6fLNKJ2K+ib5lelTdIl3AXpwf78VAgLGegH0qNOtMzjvZXW/TrBdSmSqgc26PrpfG5wghentDLW4G5u5nmZbgDnl5UI6bcm8yLp/M+Rkc6w3Kro5ro+/LOwC5byClG2v/StVmgjPWITAbgLhDXXIZ1SfT2cIUoPeodCJmsjefz4fYKyWbVAFwcX68O+LgLIVm+j6Ul7NCp9MlYAWWZeZ477L2k8v4u1fxIqppt/nT54vW3D1HU1g22BClh8sxJ9EI3LLM7erqe2ZwS5xHqNQ2/DSCs1Rxdig84zamZA5EumM5nS32Mo3yfp+vprPFi4E2rL2LGidiHgnLNhdLZ07j8/Wo4GesBg+ns8WrAS8fqkosB8sVtr+p5Hj9ImMosdWT3twN7LveHhfnx2cVVLlVISpqcx7bzwsPaupCroAo9z76ItO2v3d5dHB/28MKKvc69zRCBu2swLCVtgfNTbHLo4PXcSxIn9cXI/vccgsVZsNyWLgS5oV+Zv+r4omYwo4tSA2aL86PU3+WD1MPmhFN1XwylMl3PZDzfa7l5C/n89j28smc22PMfRJrc5h5QE/6rtjraDptSVkCs8zTLCeWZW5VlxWmlmT2XIRUXa3ouBffA6n/9D8vjw5+igq0J5dHB/uq0MZNYDYgcTJWcpT1jrvT/7ZcLnOfcG/iqYmY25c+Z6miM6ZqfhhLNp8X7CNY2o6+hcXkXI5ZS2CW86Jm2zdwxrA9yPvdnb4X9ocelqVKqkzV9bmWS74TDeNz3QAreU4+dgKzYaipFUiqPnuYJsjHEs4fL48Ozi6PDh6bnDouArOBiSUenxd8VQ+ms8WoQ5nlcpkOmicVPJW2FJaZiJlZhGep11m6IPogJmymz9/TgfU9S59z+1N+OSt2q7iYSJWaGX/9tit8B789yOpNVJaNYVv3dTlmI9dNoXsRJo6FGwHc1WnFN6B3Yvl2Wjb6bVShvYohAocj+6yPih5mA9RBP7Mvop/Z6BpuxkTM2qrszoVl3YjA+t0JYyxZ3otKleaftQ2EWNVjlWbZjWFJ5iROhvvwORjL9iCPJyPq85qrkqrU+VXO89dHI1qaqX8Zd5J64KUKrlga2Qez+HlbqXx5dPAmjidn0QvNzbEBUGE2XKX7mZ2NrZ9ZTMQ8rezC7zxDHx82FBVoZxfnx48vzo/3WlVoR1GF1qc+aPdUmWU3lh4Z2UKE6WyxzTu8epZwF4+2vD9WKeNyzJelGshn7p1kWSasYffk2ZMe9wluqtBOYhlnqkB7pAdavwnMBqqjfmZjqzA7q6zJ/9uwbD6fu8NXsVRxEMs4D1MftIvz419MJpPfpAENPeiFJjDrqRFVAPfixs0YK7JHaCz9H3NN/yxdvZ/r8XZS0/BMvxuGan8gvYFnUS33rxgi4Dy6hyzJHLB0YT6dLT4vWNY6m84WaQnC0EenNxMxH1TwVBrCsh6Li+d3F9AxlXIvThj2KqpiTL3MdkfSkwfgrtJ5UaowHnKv11xh0FeXRwd9WZZ1m31DsmB1aSljBM3fDuhtS9eND2LJ6ePdk2fanPSECrOBS/3MCo+vfzidLQZ9J63CiZjCsoFJgdSVYQKpAu3rSgYJuFPOXamuYky+iL6ygxMXtH3ty1mS701Y0+7JsxfRwmRo0hL2k8ujg0vVp/0gMBuH0mWtp1EhMzgVTsQUlo1AqkBLlZsX58fpc/Xf0f+sK3rkAazndKB9Xl3srcayTNhAVGH990CWZ16Vggf+0/0AACAASURBVLP/uTw6OLs8OhhVH/C+EZiNQEf9zAZXel7hRExh2QjFEIFU5fhfHQVnAjOA9aQLoycDfM+EQKvTuwg2sHvy7CzOPWtYZZFDGhLwKgaoUCGB2UhEj6QvC77a1LdjMGuzK5yIKSwbuVi2eRhTN0tOE9oZagUpQEafDqllheWYa/tEFQlsJibZ3o/2JEOUbqq8EJrVSWA2ItF0tmQ/s3RyOJQ7aqcVTcQUlvFOTN0sfRIhMOMu7D+M1ZCWZqouW5/3DDa0e/Lsx92TZ4+ir2/JG8Wl7AjN6iQwG5/S/cyeTGeLXn/wl8vlkyiXrYGwjPeK6bSlmqNalsldCMwYq0G0rIhKqZqGH/WFwAzuKA0D2D15dj/OeYe2TLMJzVSjVkRgNjId9TPr7R3VmIj5sIKnkjydz+f3hWVcJ03W7HggAJWyjLYufb+RxJ08mM4Wj3r+Fgp+NmNZJmxJGgiwe/KsGYZVcgVVboPsBd5nArMR6qKfWR+b3UaT/1omYqawTMNYVvGowB03J/x55AzDawrMsu0/8f22LTm3h8/QuD3ueWgqMNuc9w62KA0F2D15thfDsD4fyHLNBybr1kNgNlId9TPrzR3V5XKZLi63eeF1F18Ly1hVVJE+zvyGqY7J41XG311TQFNLP8jb5NweKv7GbSd6o/ZOVEjV0qaij/peXQhV2j15drl78uxJLNf8r1iy+bxwK6JtGuJk5V4SmI3bYeGDyFd9uKMaEzHPKpn+dDSfz51csS6l3FxVxbE389LQPvUyEZiRponnvrmRg6qHu5ldHh34/ENGEZ6lJZv7uyfPPmgFaF/3qALt3uXRgZ7BFRCYjdjF+fFlhGYlnfWgn1kNEzHfRFjWyzvQdCuqzIY4QWjocla11nKzIueF4uWWf98Ytgfv93mhAPaL6WzRtwsiFe93J3SEgloB2qNUgbZ78uwXMW3zy8qr0BxvK/DLsb8BY3dxfpwCrK8LNra/F4FUlScLy+XycQVLDd7EJMycy4EGLeNd+9MImvvgskdL3/i3nD2zaglocoYD2/5sjmF78J+O0gCV6WyRAtN/Fnh/0mPdjxsdVYvKqAf2mTs7tNwKupWmbbZvjMXxbS++n/cqOYcWrldAYEYKzR7FHc5SB4ZPUj+zi/Pjqk4Wom9Z18sfU1XQ/nw+70soU6svMj6vviyheZUx/BXmZnBxfvxqOlvk+vX3UnVvBRflvQnMCmyP3R4F8GPxNKYNN9v/y8zfJ5O4kfikJ5UELt624+2yzFT1MoQXA0MQn8efreyJJZHNTxc3C3Yujw5SRZzz7g4JzGjsx0Vwqb5dqZ/Zqy1PNLurRx33LXsZYVn1d5lHrE9VITmXPttH8znPePNiv4JG4zlPOHN8n+TcHntdbo8YxPPVln9tCpz6uoTkP557GpA0nS32C9xQTIORzlLVf+bHuSvLg7bnsEc34GCU2lVoMfBkP35KrkbadaO6W3qY8ZZ+Zm91eef06Xw+3xOWbU2uXgR7PejB17Dkq59ynhR12ispgoeccrx3ObdH19U6OY4Rfa2YuSno2y/U3+a05u+XWK5kmf/2CB+hR3ZPnv3YDBKIIQJPCz175/MdE5jxTtzZ/LrgO7JTyzS/WI55r6OHfzmfz504bVeui9ydHi1JyfkFW1Nl6NDkfG+73ndzPv55puWmObfHJx0HJKZv/dvLm6ri4oZiiXYN1ZwTXcNyzO1KE/BcCEMPxRCBwxgcUOvAALbEkkx+poN+Zg9Sg/a07KHjLdHliG9h2fZdZlz69biCZW03ikqenMuLVULmkzOg2ZnOFodNj6aSIhjKecGd633LHQ4/6mJZVmoyn+kmUR/D9Fvf/xgCUGIZzoMae7yGnKHh00qrE9O54acZf/9hBb1zgQ2lJZvR5+xFxvNuN7c6JjDjfQ4zf/CvSmPVX1TWz6yUpxr8Z5HzPb1XSch7k6wn4KkZds7fP2apmmU6W7zOWPF62FHgmzvEzfL9UWh7dHEsyXWjZshh+mF8t+Q+N3oc50TVHGejEirXZ+BNVGpUJ3oW5QzM9gVm0G+pIf/l0cHjDD1BqYTAjP8Qk6HSF/hJwXfnLMaqjy08qr3Bb1+9yDzZ7Ito0FxdcBQVojkbq7/M+Lu7UlvfoHRceJjpd6cKlr0OblDkDoVyvp6c2+Ne6YqiqPbLElAMOUxPS36jyuzbzA+1E6F2Tcv1cgZa1Z4HpZ5Fl0cHzzNWFt4zAY+OVHHeE2F8ju+/V7snz0qG0acCs+HSw4z3iiU7pZoZTnrQuyMXJ0kZFAoDXtTWoDmeT+7qoSF+TmvrI5N7GxZd7pUqMjP3iHyeqX9ZI/f2eFz4WJJrIvQQw/Sfie+WEr1eZ/G5qUXO5dS1f6fkfn4qzOhCLec9H8RN3m3/FK1aTeH6GL4Dx0pgxk3Sl/jrgu9QOkHspG/HfD7vZDmo5ZhZPc/8+3ciNOuy/907ccH9osDwiiEuna4qMIsqnZzH3llUEWcXn4/cj5X1grbA9tgptUw28/YYRVuF1Os1DZko8FBfRMVwpwosx6w9MMu9XxumQBdqOe/JNqRrQEM1DNrqmMCMa8Ud+9J3OB92eIKYO2D5D8vlUiPHfEp8waThGK+6vqhphWW5h3W8HuiSq51YalWT3AHKV9H4PbezzD2f3hQaYpD7ov6TQiHmacbtMaYq8VLVC6cVVDLnfK3VXwimaXiZA9J0YS80o7Qq9ruozMo1ZbJ09WZt7T3YEoEZN4oLkdLjcrtahtDFyb6R4vmUamyeLj6/TctnuriwibDuVaHJtlVPB72j2pbFlHivsy4rns4WpwX2y1L7ZInq5xRiZgsnYnvk6m841DD9veK1fl7goe5VcNwd83LMRu5tIDCjC7Wc9+T67vj08uigyCqQGBCS63xH+56OCcxYRekP6oNClQ8/M5/PTztYf17lZKghiArJkn34vohqsyLbNC2tigvgbwssw2x0feGWM7x/UGqZ4ipiAEru/TfbsuLYN3NOl2sUWcZfaHskJzmOIQW2x+h6kMaghhLnDJ90VQEbFSg5v1/6st/kfp4CM66T9bzn8uighvOenJWmpc5bc577C8w6JjBjFV2UzHcVJD0qXFE3Wy6XQrN8Sgc89+KC9zIqznIEEffj4vdfhQKJxtMKptjmPmn4qrJG2yX2360uK04Va2mCbKF9s/Q+Wep4ko4hZ9uo/ovjxasC26OT/qMVOCx0znDaUb/MnEHO81iOVb1Ylpm1j6FlmVwj+3nP5dFB1+c9OQPpFApm/X6K6rJc7+HrOP7QIYEZq+hiTXYnSxXn8/mrDsK6J8vlsorG8UMTE826mFpzLyrO/pUuViM82yiQiABiPw3ESEHcZDL5Z+GgrFFTkJTTFxF4Pup6oEPB/bdZVvzkLiFNVEalffST7T69axXdJwsfT9J7eLnpUu9WBeo/CyyLfVlBmN6JeN0lzhmKDYa4ImeI07dG1rmrzNw8pStfXB4dXKZqs1JLGNt2T57lHqzzMFdoFmHZC71Bh+2XY38DWEkX4VVnjRPn8/nZcrlMY+MfFnrInTgg6meWx+NYttiVWfykIGYSJwWXcdfwurvru62fUsstb/J1JRfElxl7MLWl9/yrqDh7c80d3vvx2f3y4vw4Z3BzGNWEJaRj3mEELaer9KSKMGc/Pmcl99WuKh5Lbo+dCN4fRdVe+nl13euOUD7tl3sFQ8vJiML097o4P07VgE8L3Mh4u2w8loJmFxVPOQd29O1C8DTzeeEn6eK7L1V3FFP8vOfy6ODW857dk2fbPu6fxvddLg9jaubhtiq2Lo8O9uJ5W7Y+cAIzbhQXQyUO1Fdt5Y54HMx2d0+erXVndj6fP4oJliUaqU9iaeaT9LiFHm80UlXIdLZ4XvgC8ib34qeLz9Um3lR0QdxFQLLT5bZK4ch0tigd4D+MicWv46S5OXF+FUHuB/FT8hjZ9qarZsUdbI9JbJNPm0DmSvA+aV3EdOFlVN6N3aP4POQOjVOI/6LQgIWc1WXnfVtmlKpgLo8OXmfexvsDH67D+sZy3vMkc2A2idf0r8ujg3SD40lUtq0tri0PC9wkScsxfb9WQGDGbboKcO687CXuJJxFb4hXGxwY9+MCsdSFyMPlcvkiVbgVerwxaZaKdXVR2WeHMUChBi8KnFDV6HEcj0pXGzbhbi1hc+Nxx/tkV9uj7V4l1aejri5rpP0xliSXqGY+zV2RHsuMcgZmfQ2FzjKH5QIzrhrFeU+qrIwgq0TLkU9jgubrpnK7WflxtcIzlqjuxjH3fqEbIw3fr5XQw4xrxaTKrgKzO10MxcneWSsgOYv/bWXz+bxUb5K2U/3Mti8urvUHWV9a9lZNgDvWShb778+8LLUk7Tq2xztfqy77X/FefFngoWap32Dmx7Ac8/1y7++frHuuyrCNrMLoceHBa/ciAD+Jmx0/XB4d/NT+iRYM38Zy1U8LhmWv110dRT4CM94rwrKcTQxvs/EXRKsBY/ugdm+TE7So9ipxAtzYsV49jwh+ng7xtWVy3mFgfpMuhjh0Li7Gvx7ja295k7nqZWW2x9sloe5+XxH9DM8LPNTDbU22vUbOz1lvp77tnjw7K3BBb1omV43ivCeOC2OduHyVm3IVEZjxHyoIyyZ3DI2eXNNX58Emo5Pn8/njwl9WqZ+ZC5EMLs6PD8cauKwpXRDsVbQUs220d9wuzo8fjXz/rWqfHPn2qGmpdm0OC1VJnN5lqu114qZjzmXYfb8pmPv562XLVaM574lhAiVuOtTsa73L6iIw42eiB8c/Ow7LNp5+FoHYTevfv4jJT+vaL1wm/EUMHWD79n0Z36jmsGwSFyslP4u1Gev+e1So0fm6xrg9PrcU83qxn5a46XUv04V07gqnvl/85w7MZtE3Cdr73JjOe0pfc9XkXPV2fQRmvDOdLR7HOu4ubTyR7/Lo4HDFxpinMRBgZfP5/McOyuTPlsulXhZbFkHQntDsvZqwrMZg4q3YfqMt2R/p/pvCsiovsmN7jOnk/mnXPeT6IN6j5wWe6ifT2WLb5ya5l2NW+/2yohJhsWWZvBON6Edz3I2lmWP8DKTziMOrgwfonsCMt6azxWklU1gON6kuiwBs1bBvJ0KzdYcAlGro29DPLBOh2Xud1x6WtTwZc5XZyPbfasOyRnxn7Y1gn3way9pZTcmlmVupSIrKppzLMXtfmRgXs7nDUJ8zrhrVeU8sSTyq4KmU8vaG9QBuKAySwGzkUv+L6WzxqtAY39t8vslEvgjL1j0Jm21ytyb6mZW4a9x4oJ9ZHil0uDg/vm8QwFvPexSWmVI4ntCs+rCsEZ+d3QFvD2HZmgoep3a2uMwxd1XHUG4CWpZJURHUjuoYHFMixxCaCcsqJzAbsWgW++KaBvmlbbTMI6rETjfsufbp5dHBJs1VD2NCWCn6mWUUF4FHI65YSkH1ft8aeEe4Puqpka3QrGSIX0L6LP6mL2FZY8Db43Nh2WYKHqceTGeLbTSLz7md38SUySEo8Tp85viZ+PyM6rwnQrP/HvA5+rmwrH4Cs5GKSZivKgnLzu9wMn52x9fw1eXRwVphlH5mwxMX5vdHNvEufUn/qs/9iGJK4agrBKNSMh2PPq/g6WxD+gze72tT+db2KLl8P5d0gfLfepbdWampb1/Fud1GoqIp5znhYFpMRLVP7vMFgRn/Yffk2ejOeyIoHGJF/XNhWT8IzEYoTqhexISlrr2Jg+DaLo8OUsjxYAvP/2yDfmavCl+g6meWWepDdHF+vBfVZiUrCEt7E0vd7vdlCeZNImwfQjhxJxFq/KrHJ5Rpv/wyfQY3nZJck4vz48c93x7pRH53kzYJ/FzhJeR3qcq0HHM9uV/PvXUHVDEOuyfPRnfeE6HS3kAq7N7ejNo9ebavwX8/CMxGphWWbbKEcduaiXxrHyxiKeW2+q7tbNKIdj6fl5qC1dDPrIBWtdmXAysBfxOvabdvS91uE+HEbwYedN4qBaDRl+/znu27z6OqbFDHt9b26NOx5HVUlfVumXbN4uZEiQvc2XS22LQiMHeo1/uG/1dYlklndk+eje68J4VLUWH3qx6vCEmB3+6AlqePgsBsRKazxV5FYVnyaJMKl8ujg3QC8dWWn0tqsLrJSeZh4QqC1M/MHcfMYlnV42ji3feKs9cRoKSg7PFQL4LTEr6L8+MhbK87i2qz3R4ENS+jV9n+EKrKrtM6ltS8PZoKP1VlmcR+UOIi72Gc762swHLM50OrpNg9eXZZ4PyvdPsPeiRNktw9eTa6855UbbZ78mwvAsO+BGdpGe1/pcBPVVn/CMxGYjpbpGDn24rCsi83qXKJ8vRc/VQeRhi3suhnVmp0fEM/s0IiODuNIOY38YXXh0qRN/FcfxMXwE/GUi3S0+21dZWHvk+jf95eX3uVrevK9viyou3xOvaP3aFV+FWq1PnCaQx2WtU2BgbcZKghrGWZdC41xo/gbFTnPREY7rVed21ex/f9h2kZbYTs9NAvbbThi7DspKIX+nSTE/PoM5a7Qu7J5dHBq3UaMKZ+Zsvl8lHB9/he9Clx57GguLB/e3E/nS324/3fq6QX4CS+mNPzO1Mh8h/b635sq/sRWGyj92F6vy9rX2YUQelpXEDfjwv2/Q722+dxcXk25qV+8drT99/jqAJqtkfJm1mvY1ucDqGPYZ+kSsqYZpn7fGHd8wT9yzaTXtcXmR/jkaWZrCIFSM05SQStozjvaV53tOtpzs8/6ejpvPt+1cx/OH7x008/jf09GLQKw7LzTfqWtcKyElM908Hu/rols8vl8nSLfdVW8Xn0UaNDEUK0T0xKTZ49j0m3b08UhrykLZcry5Y+iO131Y/xPjcuh/Bet0LE5mfbgc3LK/unJQg3iO2x3zqObHN7vGm2Q2wLJ/EAI3R5dLDReU8fq6Mujw72W+fn2wgM3+dn5+KqyIZJYDZgFYZlb2LZxyZN/s8K3y1I/TbWuuMayyRLhXqNX8XETioSF7+7rROR9gnKql/a7b4ML1r//NEFL9sWy7ea8PeD1j/bduNE+uoxvNk/X0WgaP+8o+lssRvvd3PseN/2uH/lombSutD5sbU9nMADMGrRq3G39X3a/HfbB3Ed977eaO1znR+jso0REJgNVKVh2d6GTf5LV241vowpNCuLhvwlByu8rYaLXmoAAADAFmj6P0DRX6mmsGxyx4mYXYRlyRdRzruyqPYq2Wui6VMCAAAAbInAbGBiKVhtAcqmEzFrCP5Oo4R3ZfP5PC0f/brgc/wkhg4AAAAAW2BJ5oBEWFZyOeAqnl+cH689fSmmu9TyWt4OKthgCMAr/cwAAACgf1SYDUSlYdn5JssTYyLmWUWvJYVem0yj3I/ebaWcxuABAAAA4A4EZgMQ081qC8tSULS/7kTMCMteRG+umnwa/dRWNp/PLyM0K2XTYA8AAABoEZj1XKVh2SQmYm4yyv5J4WWM6ziJpaIrm8/nadt8WfA5frpcLksOHQAAAIDBEZj134sKA6ajDSdiPulwIuaqXkQV3Mrm8/njyWTysuBzfLJcLtcK9gAAAID/JTDrselscVphWPb1hhMxU1XUwzxPaat2or/aukr2M9vRzwwAAAA2JzDrqels8bjCaqw0EfPRun8pljme5HlKWTyIariVzefzH/UzAwAAgH4QmPXQdLZIwcsXlT3zTSdiNtM9++bhBkMA0uv8vODr1M8MAAAANvCLn376yfvWI9PZ4n6lEzHvr9vkvzURs9Ym/7dJr3tv9+TZWv3alstlWtL5ScHneD8mdgIAAAArUGHWIzER82xAEzH7HJZNml5h6w4BiEq815me01Wb9lwDAACA0RKY9Utqpn+vsme86UTMGgcWbGIW22VlXfQzWy6X+pkBAADAigRmPRFN/kst41vV0w0nYj6qcGDBXXwSr2ll8/n8VeF+Zg+Xy2XJkA4AAAB6Sw+zHpjOFnuTyeTbyp7py4vz4711/1I0yu/TRMx1/Gb35NlaAwyWy+VpwfBQPzMAAABYgQqzyrX6ltXkfJMlhTERc8hLA88ujw521/w7j+L9LEE/MwAAAFiBwKx+pxVOxDy8OD/+cZ2/FEFSbdM9t23tQCr6mR3G+1qCfmYAAABwC4FZxaazxWGFfcv2123yH1Mka5zumcMsBhqsLPqZrdUD7Y70MwMAAIAbCMwqNZ0tditcvpgmYq7VoysMZSLmqj6NXm0rm8/n6T16WvA5ni6Xy3WXjwIAAMAoCMzqVdtSzE0nYp5WWCVXwkn0bFtH6X5ma29PAAAAGAOBWYViKeaDip5Zmoi5VsXU5H8nYpaaAFmjs1iOupLoZ7ZfsJ/Zg+Vy+bg37yYAAAAUIjCrTEzFrGkp5usNJ2Kmv3OS5yn1xr0NhgBcxhCAUr5YLpd7o91CAAAA8B4Cs/o8qWgp5pto8r/uRMz7lvu98+Dy6GCtKq75fJ5Ctq/zPq2fOVsulytXwgEAAMDQCcwqMp0t9ipbwnhoIuZWfBEVdyubz+el+5mtVQkHAAAAQyYwq0tN/aQ+vzg/XitEibDsRSxF5OdONxgCoJ8ZAAAAdEBgVonKGv2niZib9FFLf2eW4fkMwU6EZusMAbjcpH/cHehnBgAAwOhNBGZVqaW653zDiZhPRj4RcxWzdQc6zOfzVLH3ZcHnqJ8ZAAAAo/eLn376aezvQeeiuqyGiZJpIub9DZr8p4DlUb6nNTinuyfPLtd5Ucvl8kXBCsSX8/lcpRkAAACjJTCrwHS2uKyg71fqlbW3bpN/yoiqr8uCwxQ+n8/nmyzLBQAAgN6zJLNjUV1WQ5P8tSdiUs58Pk9VfyWrvr5aLpfrDikAAACAQRCYdW/tfmEZrD0Rk/Lm83kKND8v+MD6mQEAADBKArMOTWeL+xVMxtx0IiYdiGWSrws9cqp8PLWdAQAAGBuBWbe6bpR/rll/L5UMOD9ZLpf2EQAAAEZFYNat/Q4fvWnyv9ZETKpQutecfmYAAACMisCsI9PZYr/gxMOrhGWsSz8zAAAARkNg1p0uq8semYjZa11Ue+lnBgAAwGgIzLrTVWD25cX5seCj37rqKaafGQAAAKMgMOtATMfsYjlmmoj5uPM3gI0tl8vHUe3Vlcf6mQEAADB0ArNu7HXwqCZi9lxUd33R8atIQe+pfmYAAAAM2S9t3U6UDsyqaPK/XC67CAqH4H6EnV1WlrXNJpPJk8lkcti393Y6W8wnk8nvJ5PJdxfnx38q9Ji/nUwm6efj+GfbP9JzSf+8OD/+R18fe9P3dTpbfDiZTP46mUzSP3+YTCafXZwf/5DhfWie301+iPdjEu/Jd7f8+bs81rUuzo9/l/kxvrk4P15u8hdz70+ltlOJbbTm80nPZR7/+Zc7vH9/bG2XP6z7WSpxvNjGMXg6W6Tn9+f4zz9t+lld4XHWfq6tv3On49l0tvh7/OuNr+/Kvrz2Nr/yu5r954eL8+M/3PJni3+GYv/8feyfH7f+r+9b++bKx7Yr+9Gdbfu4AEAdBGbdKLmkrUhYFhVH9+NnN/75QYQrDM+ny+XyxXw+71s/vA/fcyGYRetk/KbHay5Q/zidLf5xl4vltg4ee+33NcKyv8eFT7rI+12OsGzN5/fuAnA6W3wXF6vrvicl9rG7PMba27jg/lRqOxU7Dqzi4vz4mwgr0vv80XS2+PWGYVc7RFr57xc+XmzjvW//jg/v+LtWfZxN/k4Kcja9MbPq6/tH3HSYxOdiozA8/DEeb5XfUfq79K9XQrK2j+Ln9/E5Svv/Nyv86qqOAwDUSWDWjZKVQlkmYi6Xy92olGt+aql+opwny+Xy1Xw+N3H1irj7/tfW/5ounL6Ju+DNheyHcQHw+zjZf3tBOp0tPtu0AmjS8WOv8RzbYdkkwrIsVSJXfH/LxWDzXnwUz+3vd3hPbnusbdjkMdZ6nzvanzbZTuki+S9bfpyS/hSfiY+i2mzd19Jso+/XeR/6cLzosRQufrdieLORi/Pj7yM0/vgugVlUOTbh3Dq/I+tnKMKyv7ee2z9a1Y6N5jgwj3//24r75iqBZvM7/7HJzQYA+k9gVlg0/C/l621OxFwul/sRju0LyGj1M9ubz+edLvetyZUL0B/ibvd1J+7pQupP8Xf+HBcFf53OFpNNLkS7fOw1tasFPisUlk1WDROiWudvrfckLfX5Psdj3VHWx+hwf1pnO/01Lmj/PJ0tvl8znCixjVaSqrWms8Vfosrnz7HPrfS5iKqaj+I/P1v1MXt0vOiz5viRq3p2EoHVXyPE/GiDY9WkVbX5/ZrH42yfodaNlWbJ/h9uqmqMz8/f4rvlr3E8uPbPx/t043OPY8xHsdyzimMFAGVp+l9eqWbpzy/Oj+/c5D9NRFwul6mSKAUi/zOZTB4Ky2hp+pnx8wv4SdwB//UqF5PxZ/5v6675X+N3razLx17zef61dXFWZYVKXGS1+9H8scOn04k+7E+xnX4dlSKTbfYj6shfWlVdK72WFJC0/uxy1WWSfTle9Nj3sS0/vFLBl0M7JF67r1gEU83fq+l4/OdWZdnvbtu3IwD7Xet4kPt9B2AEBGbDdH6XhuypH9lyuTxMy+0mk8k/IyTbGfubyrU+jepDfl6t8Yd17vRHBcIfWhfM657sd/nYK4mwrGluXvVyrqiyaC5EN25u3WPV70+tx2oqPz6KpWW9FK+lqRD7bVSO3eZnFWJrvO5ebN8ea1cv/X7FbbmR2B7NsXS+we9of2ayLR/dwLsQb9Wqt/ccD67rewYAKxGYDU9q8r+/SZP/CMoeTyaTy8lkcqJhP2s4jb52oxVLlZplUX/ZZFlM/J3movej+J236vKxVxUXjO1JgH1YxtVcpOVsLF6dPuxPV7Qv8j9a6W9UKpaUNq/nj1H9817xnjbVXStPZOzh9u2lWMLXVEX9MaoBc2keZ5OQqNl232y4nHPrYr9v9v11n1P7eKD6EYA7EZgNMeVbxgAADhxJREFUT5qIebnOq7oSlH2hmowNpH3mbORvXHNi/sNdep1EkNRc+K56EdrlY9+q1fdoEtUCm06Oo4yq96f3PM4PrccZQkXJn25bzheBQvOZ+seavdt6tX177g+tbfm3XC8ltn8TLK1cZRkhXvOZGURT+3Q8uDg//kX86DsGwJ0IzIblaN2JmIIytmiW+t2N8Q3N0AOm+R0f31Rh0vVjr+JKY/EUlq3clLwCzYVkTcuUsqp9f1pBFRUyd3GlGfnvr+kR9udWM/R1Gv33ffv2ypVlth/nXJrZOk6tE142+8IPNVX9XgnBVYkB0BmB2XCsNREzTTZcLpeCMrbt4Uj7mbWrWrZxl779O26rmOnysW8US4OasOybPoVl8dxrbISdW7X703WuLN/KOY2wmKiMaTfWfxdGRYDWXt68TkjYu+3bd1H91QSgf844JKE5Tn24Ri+/d8sxMz2nu2ie02+j/yUAFCcwG4aVJ2LG8su0dO5b0y7JZIz9zNoXiuuM5H+vK9PA1gnMSj/2tSJw+nvrefUpLPtt67mvPHlwIKrcn27RDgeGtK2az8xHV6qG3k233GDJWR+37xD8pT29MUc1XgSnzTa9NZSLY3TTV63GmwLtqbHz6Wzx9z4P9QCgn35pu5V1cX78YjpbbPMxV56IGZU/pyrKyGwn9rO9Mb7RqzbeHtpjt7XCsg/jIvF3lTy3D2+p7vg4LjSbP/OXO/Rbu+2x3ue7WrbhpON9eVXRg6ndy2udEKjqbZRey3S2+FO8vlSZ9I/YN5uQ404hdB+271Ck93o6W6R+Zv8vtt+fM91E+CaOYylg+tMt27gJn75f83PTlu0zlALA6Wzxu+j99lFzbJ7OFj9EMP6P+Mz3fhk2APUSmHXj9Zaqu1aaiJmqyiaTSeot9WnF7wnD8iAt+53P5y9Gtl23eeLeXByvWrXR5WO/E5UTf2stkfsoLsxqqGBoV73d5LsIy+6yTGnVx2r73ZoVUqkn0jqP8c0afYqq2J9u0hom0fTyWjfcLLGN7moZ1WUfRWVZe7rlXSrEqt++Q3MlAE2B1rrDGlaxbAXItx13m6rFuxybs36G4j37dTzX5nPQ9OF7G/hNZ4vvW+HZaPpNAlCGwKwbr7YUmO3fNhFzuVzej2qfWcXvB8OUKh/HEpg1F7E5KjZuW7rT5WO/78//PZ7Td/HfH0V1zHd3vMDfhh9uWIb2Ueu9TBeBf4sLsT9VfBH24ZoNsVe5SK1hf1olCPz4St+yzyrYv7YuKpM+i89VE0i1hwKsq6bjxeikJbSxrPDjWJr5j21W+sX+8k2ESb+9LgyL59Bsr6pDpnh/0v7efu/aAW2zZHkex+y/1DTAAIB+E5h1I/UQ++SOj5wmYt4YRiyXy8OoLLMEky6MaUlmU62xzQqL5sL2thCgy8e+qn1B/7v4Pc3SzBRA/brjZWAptPvdTX+gNUWwueBMz3uTpZn/uO2xtuC7NauqVqkqqmF/WjUI/CEu9tdtfN8osY3uLPUJa4UgkwhxN/0c1XS8GKtmaWZTjbvtfbDZV9KE1Y+u+Ww0n69v7riksehnKG5evA34WsfqZkBLc4Pmr1F9WksrAAB6TGDWjbtW3Ty9bSLmcrlMQwC+qvx9YNgMlbibnJUgt7nLY6e/84e4UGmWIDVLyXJcHG5VPO9UnbCMyWzpwuuPUSFXWyXGDz0ZSLDu/vT9DcvE2g3w/zSiSpLvmsCswv2wy2NV70Rvrua4mHpy/XGD4Q3XSvtH9PlqAqWf/e4ImprPUG+HZLSO1clnEZL9MfbHZqnorzt+mgD0nCmZHYhllC83fOSXF+fHNzb5Xy6Xp8IyKOrdHfpoeH8nV37HbRehXT72VT/EXf13lSYRaDQXbL+NEKoXLs6PP2tdUP65L8/7jmrYn1IT8r9c8/OzbZJj2uDA1XS8GK04LjbB5x+3sS2uaIKk902VbP63H4YUOMdr+XXr+PBxhGgAsDGBWXdurBC7RpqIuX/TH4iwTHN/KKu9FGndiWHv0754um2ZU5eP/R/P5X19pGI5Y3NxOO/ZRUxzQflRhovaGtW0P12nCWDblTKsppbt+9EKf2boPouQ8cOoNtum5nj78XuOW01gNrgG+VF19ofW/7SNfRyAEROYdSSWVL5e49HTRMzDmyZiCsuozDr7d69FD5imcuN9d/TX1YQAt4777/Kx1/RZ64L6rz0Kn9pVM4OvZurD/hTLUJuL/VRlJnxZUUXb9y7brP057G1V25VwJwVbf9zi7/6udbx9t53js9KESINczhzva1NlpgIVgDsRmHXr8RqPniZivrru/xSWUaGxTMhsNBcf6cJn47va8XebMGnVCoAuH3slcRHzWesC9++W01Wr+v3pyrCDsSyX3ZYut297Seimj/3u7/V9MmqEv03F5J/vsj3eo9km7WC0+fdt3xDZqjQldzpb/DSdLf5W63MEYBwEZh2KKrPzFZ7BjRMxhWVU6snINsyyFQbd5QK+/XdXrQDo8rFXFhdon8Wf/zCaMtdujD2aqt+folKqCc1+v+WgYei63L7tJvNrV7i1JiNOBrSk8C9Xqm+3dSOhvZy8ec/mV/6/WjXB6m83fD8MogBgKwRm3buxgf9tEzGFZVTq+Xw+v7YicoiigqqpFPh4k+b28XeagOZPq4777/Kx1xUT/pqgY6PnWsqVaXJVV2RsU4/2p20FP6NSwbGqCWvmrSBnVX9uLbMbxJLCVvXtJIKerRwT4/c2oeJvYxl8EyTVHjY2z2/tPoWxTzWvs7dTQAGog8CsY7HM8strnsWNEzGXy+WhsIwKvVkhCB6kNNWvdYKeLgb/tsrd8fRnYulJc2HwXfyulXX52OuK39++aN5a755tiV4/f29deGV9T2rTh/3pPcGPAQAr6nj7/qUVdP511dAsQrrmcb+J5YyDEGH8u4rJLb6m9rLMd5V5uW6IbEts2/Y03JW+IyIUbALH74c42ACAsn7p/e7exfnx4+lssTeZTB60nszrmyZiLpfL9OdPRvD20C8pLNubz+fXDqeoxIcbLOH6YcUKoz9E0PJxXKCkO/tvL06v/v04uU/P44+tqon0Z3634dvU5WOv60/xPD+OC6LvClwAr7LdP2q9f+8qWS7Oj9etZtlkH5tUFgJUvz+lsCaCso9iP/omgrRVDGEb3UUn2zeFNdPZ4nfx2Ol3pbDuH/E5+1nA0WpS/8dWeN1e2j0YsS//dpuTHdP7OZ0tmkmcTei0zf0352eovX82/d2+icDvZ5/x2D/nrUD17UCFNY4FAPBeArN67EeT9FmEDvvXTcRcLpe7k8nkbKTvE/V6mfbbHoRlkzgBX7d/1j9WuTiME/RfT2eLP7cuLt8uF5vOFpNWb5b3TYlLF6t/2fQkv8vH3uS5TmeLdEH0/1oXzb/OXPmwyXb/04YVUps8VvKLDf5OFj3an1L4+rfW8q1Vt1fvt9FddHys+i5Cs7/F7/9tBHbNH/nuSv/AxjI+k0MNQj5rHRO3ZdkKy37YIPy/SbbPUHxHNPvIb1s/qSqxPe31amD3fYRlo1hCD0BelmRWIsKxw2Y5200TMSMs2xnVG0St0v76dDKZ/GY+n/ehsqyYi/PjdBH/f6/0WZrExWH7ArTp6fN/09/ZxoVgl4+95vP8PqoIJq3QrOvJmc0ynvQe/p/cy1P7oPb9KaqS2su33he0cP3718n2jUDj1/FZuxpuXN2GaRv/7uL8+LMhVw3FMXHb1XPfXPPv1Uvb+uL8+HfxnrSr0j5qBWiNZhDIr4VlAGzLL3766SdvJkBmcRH/YetC8Ls1lnneSZePzfDYn4atq+0byy+bnw8jAPlhSL3KuLvWEtCPW0Hrd5ZfApCDwAwAAAAAWizJBAAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIAWgRkAAAAAtAjMAAAAAKBFYAYAAAAALQIzAAAAAGgRmAEAAABAi8AMAAAAAFoEZgAAAADQIjADAAAAgBaBGQAAAAC0CMwAAAAAoEVgBgAAAAAtAjMAAAAAaBGYAQAAAECLwAwAAAAAWgRmAAAAANAiMAMAAACAFoEZAAAAALQIzAAAAACgRWAGAAAAAC0CMwAAAABoEZgBAAAAQIvADAAAAABaBGYAAAAA0CIwAwAAAIDGZDL5/wFTr234bVyeJgAAAABJRU5ErkJggg==';
          return base64_logo_img;
      },

      scrollToTop(class_name){
        var elmnt = document.getElementsByClassName(class_name);
        scrollTo(elmnt, 0, 100);
      },
    }
  }
