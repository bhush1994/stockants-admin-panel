import axios from 'axios';
// import CripNotice from 'crip-vue-notice';
import router from './router';

// production addr
// export const productionAddr = 'https://prod-api.stockants.com/api';
// export const serverHostName = 'https://prod-api.stockants.com';

// staging addr
export const productionAddr = 'https://dev-api.stockants.com/api';
export const serverHostName = 'https://dev-api.stockants.com';


export const serverAddr = axios.create({
  baseURL: productionAddr,
  timeout: 300000,
  headers: {},
  withCredentials: true,
  transformRequest: [function (data, headers) {
    // Do whatever you want to transform the data
    const access_token = window.localStorage.getItem("token");

    if(access_token){
      // data.token = access_token;
      headers['Authorization'] = 'Bearer ' + access_token;
    }

    headers['Content-Type'] = "application/json;charset=UTF-8";
    return JSON.stringify(data);
  }],

  transformResponse: [function(data) {
    data = JSON.parse(data);
    if(!data.status && data.redirect){
      // console.log('transformResponse', data);
      router.push({ path: data.redirect });
      localStorage.clear();
      return;
    }

    if ("token_data" in data){
      // delete serverAddr.defaults.headers['access-token'];
      window.localStorage.setItem("token", data["token_data"]["access_token"]);
      window.localStorage.setItem("refresh_token", data["token_data"]["refresh_token"]);

      if ("expire_in" in data){
        window.localStorage.setItem("expire_in", data["expire_in"]);
      }
    }

    if (!data.status && data.validation) {
      // new CripNotice({
      //   title: "Oops! Something went wrong!",
      //   description: data.validation,
      //   type: "error",
      //   duration: 3,
      // });
    }
    return data;
  }]
});
